DESTDIR="/var/www/html/repo/tutos/19"
mkdir -p /var/www/html/repo/tutos/19/{SRPMS,x86_64,arm,x86,arm_64}

for ARCH in x86_64 
do
    pushd ${DESTDIR}/${ARCH} >/dev/null 2>&1
        createrepo .
    popd >/dev/null 2>&1
done
